# pixel7_configuration

## General configuration
### Enable Developer Mode
Settings -> About phone  
- tape multiple times on: Build number
- unlock
- reboot phone

### Enable USB debugging
Settings -> System -> {} Developer options  
- USB debugging: on

### Allow direct file tranfer from computers
- connect your phone via USB cable to a computer  
Settings -> System -> {} Developer options -> Default USB pixel7_configuration
- File transfer / Android Auto

### Navidation menu with 3 buttons
Settings -> System -> Gestures -> System navigation  
- 3-button navigation: On  

### Change time to 24-hour format
Settings -> System -> Date & Time  
Use 24-hour format: On  

### Enable flashlight on the go
Settings -> System -> Gestures -> Quick Tap to start actions  
- Use Quick Tap: On  
- Toggle flashlight  
- require stronger taps: On  

### Customized Keyboard
Settings -> System -> Languages & Inputs -> On-screen keyboard -> Gboard -> Preferences  
- Number row: On  
- disable Haptic feedback on keypress  
- Keyboard height: Mid-tall  

### Change the home screen launcher grid size
Settings -> Wallpaper & style -> App grid  
- select 5x5  

### Customize lock screen
Settings -> Display -> Lock screen  
- Wake screen for notification: On  
- Privacy: Show sensitive content only when unlocked  
- Add text on lock screen: \<email address (phone number)\>  

### Allow app pinning
Settings -> Security -> Advanced settings -> App pinning  
- Use app pinning: On  
- Ask for unlock pattern before unpinning: On  

### Remove Google Feed from the Home screen
- Long press on the home screen  
- Home Settings  
- Swipe to access Google app  

## Personalize Google assistant
### Make Google assistant obey your voice only  
- click on the app icon: Home  
settings -> Google Assistant -> Manage all assistant settings -> Hey Google & Voice Match  
- active "Hey Google" if not yet done  
If it was not active, it will ask you to read some sentences,  
- else tap: Retrain voice model  
  
Now it will obey only to your voice  

### Toggle the Quick phrase "Hey Google" for calls and clock
settings -> Google Assistant -> Manage all assistant settings -> Quick phrases  
- Select: This device  
- Alarms and timers: On  
- Incoming calls: On  
  
Now you can simply stop the morning clock by saying: "Stop", or "Snooze"  
and manage your phone calls handfree by simply saying: "Answer", "Decline" or "Silence"  

### Set the default waether temperature to degrees celsius
settings -> Google Assistant -> Manage all assistant settings -> Weather  
- Celsius: On  

## Security / Privacy
### Google location history
Settings -> Privacy -> Google location history  
- Location History: Off  
- Deleteing activity older than 3 months
- You may also check/disable the web app activity

### Disable debug logging for ads
Settings -> Privacy -> Ads  
- Enable debug logging for ads: Off  

### Manage authorizations
Settings -> Privacy -> Permission Manager  
- For each service, disable the apps that do not need it !  

### Usage & diagnostics
Settings -> Privacy -> Usage & diagnostics  
- Usage & diagnostics: Off  

### Install unknown apps (prevent apps to auto install other apps/files)
Settings -> Apps -> Special app access  -> Install unknown apps  
- Disable all apps  

### Control Wifi
Settings -> Apps -> Special app access  -> Wi-Fi control  
- Disable any apps you don't want it to use Wi-Fi even when it's switched Off !  
  
I would switch them all Off, then switching the Wi-fi Off Global makes more sens now.  

## Battery Utilization and Energy Consumption
### Wi-Fi scan throttling
Settings -> System -> {} Developer options  
- Wi-Fi scan throttling: On  

### Adaptive preferences
Search in settings for: "Adaptive preferences"  
- Adaptive Charging: On  
- Adaptive Battery: On  

### Dark Mode
Settings -> Display  
- Dark theme: On  
Settings -> System -> {} Developer options  
- Override force-dark: On  
  
Optionally you should set the darkest background images as possible  

### Background apps to force shutdown in Sleep Mode
Settings -> System -> {} Developer options -> Background check  
- disable all apps  

### Prevent apps to use data in the Background
rather than goin in each app setting  
Settings -> Network & Internet  
- Use Data Saver: On  
